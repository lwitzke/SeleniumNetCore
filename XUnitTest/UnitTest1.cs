using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.PhantomJS;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;
using System.Reflection;
using Xunit;

namespace XUnitTest
{
    public class UnitTest1
    {
        
        [Fact]
        public void TestIfPageIsOnline()
        {
            using (var driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)))
            {
                driver.Navigate().GoToUrl(@"https://www.google.com.ar/");
                IWebElement passwordTextBox = driver.FindElement(By.Id("lst-ib"));
                passwordTextBox.SendKeys("We Are Around");

                driver.FindElement(By.Name("btnK")).SendKeys(Keys.Enter);

                var wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
                var clickableElement = wait.Until(ExpectedConditions.ElementToBeClickable(By.PartialLinkText("We are Around - The Broadcast Revolution")));
                clickableElement.Click();

            }
        }      

    }
}
